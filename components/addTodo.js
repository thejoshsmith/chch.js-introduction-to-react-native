import React, { Component } from 'react'
import { connect } from 'react-redux'

import { View, Input, Button, Text, Form, Item, Label, Toast } from 'native-base'

// Action
import * as todoActions from '../actions/todos'

class AddTodo extends Component {

	state = {
		todoText: ''
	}

	onFormSubmit = () => {

		const { todoText } = this.state

		if (todoText === '') {
			return Toast.show({
				text: "Please enter a Todo.",
				type: "warning"
			})
		}

		this.props.addTodo(todoText)
		this.setState({todoText: ''})
	}

	render() {

		const { todoText } = this.state

		return (
			<Form style={{padding: 16}}>
				<Item floatingLabel>
					<Label>Enter your todo</Label>
					<Input onChangeText={(text) => this.setState({ todoText: text })} value={todoText} />
				</Item>
				<Button block style={{marginTop: 16}} onPress={() => this.onFormSubmit()}>
					<Text>Add Todo</Text>
				</Button>
			</Form>
		)
	}

}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
	addTodo: (text) => dispatch(todoActions.setTodo(text))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo)