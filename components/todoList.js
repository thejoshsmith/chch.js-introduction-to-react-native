import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as select from '../reducers'
import * as todoActions from '../actions/todos'

import { List, ListItem, CheckBox, Body, Text } from 'native-base'

class TodoList extends Component {

	handleTodoClick = (id) => {
		this.props.toggleTodo(id)
	}

	render() {

		const { todos } = this.props

		return (
			<List style={{marginTop: 32}}>
				{todos.map(todo =>
					<ListItem
						key={todo.id}
						onPress={() => this.handleTodoClick(todo.id)}
						className={todo.completed ? 'completed' : ''}
					>
							<CheckBox checked={todo.completed} type="MaterialIcons" />
							<Body>
								<Text style={{ textDecorationLine: todo.completed ? 'line-through' : 'none'}}>{todo.text}</Text>
							</Body>
					</ListItem>
				)}
			</List>
		)
	}

}

const mapStateToProps = (state) => ({
	todos: select.getTodos(state)
})

const mapDispatchToProps = (dispatch) => ({
	toggleTodo: (id) => dispatch(todoActions.toggleTodo(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)