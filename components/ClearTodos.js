import React, { Component } from 'react'
import {Button, Text, Toast} from 'native-base'

import { connect } from 'react-redux'
import * as todoActions from '../actions/todos'

class ClearTodos extends Component {

	clearTodos = () => {

		this.props.clearAll()

		Toast.show({
			text: "Todo's have been cleared",
			type: 'success'
		})
	}

	render() {

		return (
			<Button onPress={this.clearTodos} transparent danger>
				<Text style={{fontWeight: 'bold'}}>Clear Todos</Text>
			</Button>
		)
	}
}

const mapDispatchToProps = (dispatch) => ({
	clearAll: () => dispatch(todoActions.clearAll())
})

export default connect(null, mapDispatchToProps)(ClearTodos)