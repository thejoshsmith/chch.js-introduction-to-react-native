let nextTodoId = 0

export const setTodo = (text) => ({
	type: 'ADD_TODO',
	payload: {
		text,
		id: nextTodoId++
	}
})

export const toggleTodo = (id) => ({
	type: 'TOGGLE_TODO',
	payload: { id }
})

export const clearAll = () => ({
	type: 'CLEAR_ALL'
})