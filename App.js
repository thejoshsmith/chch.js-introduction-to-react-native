import React from 'react';

import {store, persistor} from './store'
import ClearTodos from './components/ClearTodos'

import {
	View,
	Text,
	Container,
	Content,
	Header,
	Body,
	Left,
	Right,
	Title,
	Button,
	Icon,
	Root,
	Footer
} from 'native-base'

// Components
import AddTodo from './components/addTodo'
import TodoList from './components/todoList'

import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

class App extends React.Component {

	render() {
		return (
			<Root>
				<Provider store={store}>
					<PersistGate loading={null} persistor={persistor}>
						<Container>
							<Header>
								<Left />
								<Body>
									<Title>Todo App</Title>
								</Body>
								<Right>
									<Button transparent>
										<Icon type="FontAwesome" name="bars" />
									</Button>
								</Right>
							</Header>
							<Content>
								<AddTodo />
								<TodoList />
							</Content>
							<Footer>
								<ClearTodos />
							</Footer>
						</Container>
					</PersistGate>
				</Provider>
			</Root>
		)
	}

}

export default App